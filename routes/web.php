<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->group(['prefix' => 'api/v1/'], function ($app) {
    $app->get('login/','UsersController@authenticate');
    $app->get('checklists/', 'ChecklistsController@index');
    $app->get('checklists/{id}', 'ChecklistsController@show');
    $app->post('checklists', 'ChecklistsController@store');
    $app->patch('checklists/{id}', 'ChecklistsController@update');
    $app->delete('checklists/{id}', 'ChecklistsController@destroy');
});