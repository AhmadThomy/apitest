<?php
namespace App\Http\Controllers;

use App\ChecklistsModel;
use Illuminate\Http\Request;

class ChecklistsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

        $this->middleware('auth');
    
    }

    public function index(){
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $page_numb = ($page - 1) * $limit;
        $next = $page_numb + 1;
        $prev = $page_numb - 1;
        
        $data = ChecklistsModel::skip($page_numb)->take($limit)->get();
        $total = ChecklistsModel::count();
        foreach ($data as $key => $value) {
            $listData[$key] = $this->getData($value->id);
        }
        $count_pages = ceil($total / $limit);
        return response()->json([
            'meta' => ['count' => $limit,'total' => $total],
            'links' => [
                'first' => 'http://localhost:8000/api/v1/checklists?page=1&limit='.$limit,
                'last' => 'http://localhost:8000/api/v1/checklists?page='.$count_pages.'&limit='.$limit,
                'next' => $next < $count_pages ? 'http://localhost:8000/api/v1/checklists?page='.$next.'&limit='.$limit : 'null',
                'prev' => $prev > 0 ? 'http://localhost:8000/api/v1/checklists?page='.$prev.'&limit='.$limit : 'null'
            ],
            'data' =>  $listData
        ]);
    }

    public function getData($id){
        $dataHeader = ChecklistsModel::where('id',$id)->get(['type','id'])->first();
        $dataDetail = ChecklistsModel::where('id',$id)->get([
            'object_domain',
            'object_id',
            'description',
            'is_completed',
            'due',
            'task_id',
            'urgency',
            'completed_at',
            'last_update_by',
            'update_at',
            'created_at'
        ])->first();
    
        return [
            'type' => $dataHeader->type, 
            'id' => $dataHeader->id, 
            'attributes' => $dataDetail,
            'links' => ['self' => 'http://localhost:8000/api/v1/checklists/'.$dataHeader->id]
        ];
    }

    public function show($id){
        return response()->json(['data' => $this->getData($id)]);
    }

    public function store (Request $request){
        $data = new ChecklistsModel();
        $data->type = $request->input('type', false);
        $data->object_domain = $request->input('object_domain', false);
        $data->object_id = $request->input('object_id', false);
        $data->description = $request->input('description', false);
        $data->is_completed = $request->input('is_completed', false);
        $data->due = $request->input('due', false);
        $data->task_id = $request->input('task_id', false);
        $data->urgency = $request->input('urgency', false);
        $data->completed_at = $request->input('completed_at', false);
        $data->last_update_by = $request->input('last_update_by', false);
        $data->update_at = $request->input('update_at', false);
        $data->created_at = $request->input('created_at', false);
        $data->save();

        $dataDetail = ChecklistsModel::where('id',$data->id)->get([
            'object_domain',
            'object_id',
            'description',
            'is_completed',
            'due',
            'task_id',
            'urgency',
            'completed_at',
            'last_update_by',
            'update_at',
            'created_at'
        ]);
    
        return response()->json(['data' => ['attributes' => $dataDetail]]);
    }

    public function update(Request $request, $id){
        $data = ChecklistsModel::where('id',$id)->first();
        $data->type = $request->input('type', false);
        $data->object_domain = $request->input('object_domain', false);
        $data->object_id = $request->input('object_id', false);
        $data->description = $request->input('description', false);
        $data->is_completed = $request->input('is_completed', false);
        $data->due = $request->input('due', false);
        $data->task_id = $request->input('task_id', false);
        $data->urgency = $request->input('urgency', false);
        $data->completed_at = $request->input('completed_at', false);
        $data->last_update_by = $request->input('last_update_by', false);
        $data->update_at = $request->input('update_at', false);
        $data->created_at = $request->input('created_at', false);
        $data->save();
    
        return $this->getData($id);
    }
    
    public function destroy($id){
        $data = ChecklistsModel::where('id',$id)->first();
        $data->delete();
    
        return response('Berhasil Menghapus Data');
    }
}
