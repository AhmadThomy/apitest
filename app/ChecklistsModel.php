<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class ChecklistsModel extends Model{
   public $timestamps = false;
   protected $table = 'checklists';
}