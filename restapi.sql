/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : localhost:3306
 Source Schema         : restapi

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 28/06/2019 20:54:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for checklists
-- ----------------------------
DROP TABLE IF EXISTS `checklists`;
CREATE TABLE `checklists`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `object_domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `object_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `due` date NOT NULL,
  `task_id` int(11) NOT NULL,
  `urgency` tinyint(4) NOT NULL,
  `completed_at` datetime(0) NOT NULL,
  `last_update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `update_at` datetime(0) NOT NULL,
  `created_at` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of checklists
-- ----------------------------
INSERT INTO `checklists` VALUES (1, 'checklists', 'contact', '0', '0', 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `checklists` VALUES (2, '0', 'contact', '0', '0', 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `checklists` VALUES (3, 'checklists', '0', '0', '0', 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `checklists` VALUES (4, '0', 'Kontak', '0', '0', 0, '0000-00-00', 0, 0, '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2019_06_27_080436_table_restapi', 1);
INSERT INTO `migrations` VALUES (2, '2019_06_28_063816_create_checklist_table', 2);
INSERT INTO `migrations` VALUES (3, '2019_06_28_132231_create_users_table', 3);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `userimage` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `users_api_key_unique`(`api_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'ahmad Thomy', 'ahmad10thomy@gmail.com', '$2y$12$3xaEeJ/rBGperg5KnIU3iOSlfxIaDHY1fkCJmOqURfl1/DsvjkPqq', NULL, 'VjBZU04xVVJSbGJ4S1JRazYzOW04Z1EwOW1mSEJGTWV0TnpUV1JHTw==', NULL, NULL, '2019-06-28 20:41:47');

SET FOREIGN_KEY_CHECKS = 1;
